const fs = require("fs");
const path = require("path");
function toMakeDirectory(
  directoryName,
  numberOfFiles,
  cbToWriteFiles,
  cbToDeleteFiles
) {
  fs.mkdir(directoryName, (err) => {
    if (err) console.log(err);
    else {
      console.log("Created Directory Successfully!!");
      cbToWriteFiles(directoryName, numberOfFiles, cbToDeleteFiles);
    }
  });
}
function cbToWriteFiles(directoryName, numberOfFiles, cbToDeleteFiles) {
  for (let i = 1; i <= numberOfFiles; i++) {
    fs.writeFile(
      path.join(`${directoryName}`, `file${i}.json`),
      `This is File${i}`,
      (err) => {
        if (err) {
          console.error(err);
        }
        console.log(`File${i} Created Successfully!`);
      }
    );
    if (i == numberOfFiles) {
      cbToDeleteFiles(directoryName, numberOfFiles);
    }
  }
}
function cbToDeleteFiles(directoryName, numberOfFiles) {
  for (let i = 1; i <= numberOfFiles; i++) {
    fs.unlink(`./${directoryName}/file${i}.json`, (err) => {
      if (err) console.log(err);
      console.log(`File${i} Deleted`);
    });
  }
}
module.exports = { toMakeDirectory, cbToWriteFiles, cbToDeleteFiles };
