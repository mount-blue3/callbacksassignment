const fs = require("fs");
const path = require("path");
function callbackhell(dataPath) {
  console.log("Reading lipsum file");
  fs.readFile(dataPath, "utf-8", (err, data) => {
    if (err) {
      console.error(err);
      return;
    } else {
      let dataToUpperCase = data.toUpperCase();
      fs.writeFile(
        path.join(__dirname, "file1.txt"),
        dataToUpperCase,
        (err) => {
          if (err) {
            console.error(err);
            return;
          } else {
            console.log("Uppercase data is successfully written into file1");
            return;
          }
        }
      );
      fs.appendFile(
        path.join(__dirname, "fileNames.txt"),
        "file1.txt",
        (err) => {
          if (err) {
            console.error(err);
            return;
          } else {
            console.log("file1 appended to filenames");
            return;
          }
        }
      );
      fs.readFile("../file1.txt", "utf-8", (err, data) => {
        if (err) {
          console.error(err);
          return;
        } else {
          console.log("file1 is readed");
          let dataToLowerCase = data.toLowerCase().split(".");
          fs.writeFile(
            path.join(__dirname, "file2.txt"),
            dataToLowerCase,
            (err) => {
              if (err) {
                console.error(err);
                return;
              } else {
                console.log("sentences are written into file2 successfully");
                return;
              }
            }
          );
          fs.appendFile(
            path.join(__dirname, "fileNames.txt"),
            "\nfile2.txt",
            (err) => {
              if (err) {
                console.error(err);
                return;
              } else {
                console.log("file2 appended to filenames");
                return;
              }
            }
          );
          fs.readFile("../file2.txt", "utf-8", (err, data) => {
            if (err) {
              console.error(err);
              return;
            } else {
              console.log("file2 readed successfully");
              let sortedData = data.split(",").sort();
              fs.writeFile(
                path.join(__dirname, "file3.txt"),
                sortedData,
                (err) => {
                  if (err) {
                    console.error(err);
                    return;
                  } else {
                    console.log("sorted data written successfully into file3");
                    return;
                  }
                }
              );
              fs.appendFile(
                path.join(__dirname, "fileNames.txt"),
                "\nfile3.txt",
                (err) => {
                  if (err) {
                    console.error(err);
                    return;
                  } else {
                    console.log("file3 appended to filenames");
                    return;
                  }
                }
              );
            }
            fs.readFile("../fileNames.txt", "utf-8", (err, data) => {
              if (err) {
                console.error(err);
                return;
              } else {
                console.log(
                  "filenames readed successfully and started deleting files"
                );
                let splitData = data.split("\n");
                for (let i = 0; i < splitData.length; i++) {
                  fs.unlink(`../${splitData[i]}`, (err) => {
                    if (err) {
                      console.log(err);
                      return;
                    } else {
                      console.log(`${splitData[i]} Deleted`);
                      return;
                    }
                  });
                }
              }
            });
          });
        }
      });
    }
  });
}

module.exports = callbackhell;
