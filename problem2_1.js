const fs = require("fs");
const path = require("path");

//Function to readfiles

function toReadFile(PathtoReadFile, callback) {
  fs.readFile(PathtoReadFile, "utf-8", (err, data) => {
    if (err) callback(err);
    else {
      callback(null, data);
    }
  });
}

// function to write content into files

function toWriteFile(pathtoWriteFile, content, callback) {
  fs.writeFile(pathtoWriteFile, content, (err) => {
    if (err) callback(err);
    else {
      callback(null);
    }
  });
}

//function to append filenames

function toAppendFile(pathtoAppendFile, content, callback) {
  fs.appendFile(pathtoAppendFile, content, (err) => {
    if (err) callback(err);
    else {
      console.log(`Appended file successfully`);
      callback(null);
    }
  });
}

//function to delete a file

function toDeletefile(path) {
  fs.unlink(path, (err) => {
    if (err) {
      console.error(err);
      return;
    } else {
      console.log("Deleted file successfully");
      return;
    }
  });
}

//function to delete more number of files
function toDeleteFiles(path, readfile, deleteFile) {
  readfile(path, (err, data) => {
    if (err) {
      console.error(err);
      return;
    } else {
      console.log("deleting files");
      const filenames = data.split("\n").filter((filename) => {
        return filename !== "";
      });
      filenames.forEach((file) => {
        deleteFile(`../${file}`);
      });
    }
  });
}

//function to convert data into uppercase
function toUpperCase(pathtoread, readfile, writefile, appendfile) {
  readfile(pathtoread, (err, data) => {
    if (err) {
      console.error(err);
      return;
    } else {
      console.log("lipsum is readed");
      const pathtofile1 = path.join(__dirname, "file1.txt");
      let upperCaseData = data.toUpperCase();
      //writing uppercase data into file1
      writefile(pathtofile1, upperCaseData, (err) => {
        if (err) {
          console.error(err);
          return;
        } else {
          console.log("successfully created the file to store uppercase data");
          const pathtoAppend = path.join(__dirname, "fileNames.txt");
          //appending file1 to filenames
          appendfile(pathtoAppend, "file1.txt\n", (err) => {
            if (err) {
              console.error(err);
              return;
            } else console.log("Done the first problem");
            toSentences(pathtofile1, toReadFile, toWriteFile, toAppendFile);
          });
        }
      });
    }
  });
}
//function to convert into sentences
function toSentences(pathtoread, readfile, writefile, appendfile) {
  //reading file1
  readfile(pathtoread, (err, data) => {
    if (err) {
      console.error(err);
      return;
    } else {
      console.log("file1 is readed");
      const pathtofile2 = path.join(__dirname, "file2.txt");
      let sentenceData = data.toLowerCase().replace(/[.!?]+/g, "\n");
      //writing sentences into file
      writefile(pathtofile2, sentenceData, (err) => {
        if (err) {
          console.error(err);
          return;
        } else {
          console.log("successfully created the file to store sentences");
          const pathtoAppend = path.join(__dirname, "fileNames.txt");
          //appending file2 to filenames
          appendfile(pathtoAppend, "file2.txt\n", (err) => {
            if (err) {
              console.error(err);
              return;
            } else console.log("Done the second problem");
            dataToSort(
              pathtofile2,
              toReadFile,
              toWriteFile,
              toAppendFile,
              toDeleteFiles
            );
          });
        }
      });
    }
  });
}
// function to sort data

function dataToSort(pathtoread, readfile, writefile, appendfile, deletefile) {
  //reading file2
  readfile(pathtoread, (err, data) => {
    if (err) {
      console.error(err);
      return;
    } else {
      console.log("file2 readed successfully");
      const pathtowrite = path.join(__dirname, "file3.txt");
      let sortedData = data.split("\n").sort();
      //writing sorted data into file3
      writefile(pathtowrite, sortedData, (err) => {
        if (err) {
          console.error(err);
          return;
        } else {
          console.log("successfully created the file to store sorted data");
          const pathtoAppend = path.join(__dirname, "fileNames.txt");
          //appending file3 to filenames
          appendfile(pathtoAppend, "file3.txt\n", (err) => {
            if (err) {
              console.error(err);
              return;
            } else console.log("Done the third problem");
            //deleting files
            deletefile(pathtoAppend, toReadFile, toDeletefile);
          });
        }
      });
    }
  });
}

module.exports = {
  toReadFile,
  toWriteFile,
  toAppendFile,
  toUpperCase,
};
