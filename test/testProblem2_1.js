const {
  toReadFile,
  toWriteFile,
  toAppendFile,
  toUpperCase,
} = require("../problem2_1");
const path = require("path");
const pathToLipsum = path.join(__dirname, "../lipsums.txt");
toUpperCase(pathToLipsum, toReadFile, toWriteFile, toAppendFile);
